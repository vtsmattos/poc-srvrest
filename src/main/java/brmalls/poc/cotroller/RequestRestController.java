package brmalls.poc.cotroller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import brmalls.poc.model.ClientesInadimplentes;
import brmalls.poc.proxy.ClientesInadimplentesRestProxy;

@RestController
@RequestMapping("/rest")
public class RequestRestController {

	@Autowired
	ClientesInadimplentesRestProxy clientesInadimplentesProxy;
	
	@SuppressWarnings("deprecation")
	@GetMapping("/get")
	public List<ClientesInadimplentes> requestGet(){
		String shopping = "NORTESHOPPING";
		Date dataInicio = new Date(119,3,1);//1/4/2019
		Date dataFim = new Date(119,3,30);//30/4/2019
		
		List<ClientesInadimplentes> clientes = clientesInadimplentesProxy.resquestGet(shopping, dataInicio, dataFim);
		return clientes;		
	}
	
	@GetMapping
	public String ok(){
		return "OK";
	}
	

}
