package brmalls.poc.proxy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import brmalls.poc.model.ClientesInadimplentes;

@Service
public class ClientesInadimplentesRestProxy {

	@Autowired
	private Environment env;
	
	//http://localhost:54376/api/ClienteInadiplente2/get/?shopping=NORTESHOPPING&dataInicio=01-04-2019&dataFim=30-04-2019

	@Autowired
	private RestTemplate restTemplate;

	@HystrixCommand(fallbackMethod = "resquestGetFallback", commandProperties = {
			@HystrixProperty(name = "execution.isolation.strategy", value = "THREAD"),
			@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
			@HystrixProperty(name = "requestCache.enabled", value = "false"), }, threadPoolProperties = {
					@HystrixProperty(name = "coreSize", value = "5"),
					@HystrixProperty(name = "maximumSize", value = "5") })
	public List<ClientesInadimplentes> resquestGet(String shopping, Date dataInicio, Date dataFim) {

		try {
			String url = env.getProperty("environments.url");
			if(Objects.isNull(url)){
				//se não conseguir ler do config server
				url = "http://localhost:54376/api/ClienteInadiplente2/get/";
			}
			
			url += "?shopping=" + shopping; 
			url += "&dataInicio=" + formatar(dataInicio, "dd-MM-yyyy");
			url += "&dataFim=" + formatar(dataFim, "dd-MM-yyyy");
			
			ResponseEntity<List<ClientesInadimplentes>> response = restTemplate.exchange(url, HttpMethod.GET, null,new ParameterizedTypeReference<List<ClientesInadimplentes>>() {});
			List<ClientesInadimplentes> todoList = response.getBody();
			return todoList;

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	private String formatar(Date data, String format) {
		SimpleDateFormat simple = new SimpleDateFormat(format); 
		return simple.format(data);		
	}

	List<ClientesInadimplentes> resquestGetFallback(String shopping, Date dataInicio, Date dataFim) {
		return new ArrayList<ClientesInadimplentes>();
	}

}
